package org.example;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        String text = "Hello world!";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String timestamp = dateFormat.format(new Date());

        String fileName = "output_" + timestamp + ".md";
        //String fileName = "output.md";

        File outputFile = new File(fileName);

        try {
            FileWriter writer = new FileWriter(outputFile);
            writer.write(text);
            writer.close();
            System.out.println("Markdown file generated successfully: " + fileName);
        } catch (IOException e) {
            System.out.println("An error occurred while generating the Markdown file.");
            e.printStackTrace();
        }
    }
}
